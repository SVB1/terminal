﻿namespace Terminal
{
    partial class MainWindow
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;



        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolBtnDisconnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolBtnСonPort = new System.Windows.Forms.ToolStripButton();
            this.toolBtnСonModem = new System.Windows.Forms.ToolStripButton();
            this.toolBtnLogin = new System.Windows.Forms.ToolStripButton();
            this.toolButtonPdt = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actToolStripMenuItem,
            this.setToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(462, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actToolStripMenuItem
            // 
            this.actToolStripMenuItem.Name = "actToolStripMenuItem";
            this.actToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.actToolStripMenuItem.Text = "Действия";
            // 
            // setToolStripMenuItem
            // 
            this.setToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.terminalToolStripMenuItem,
            this.comPortToolStripMenuItem,
            this.modemToolStripMenuItem});
            this.setToolStripMenuItem.Name = "setToolStripMenuItem";
            this.setToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.setToolStripMenuItem.Text = "Настройки";
            // 
            // terminalToolStripMenuItem
            // 
            this.terminalToolStripMenuItem.Name = "terminalToolStripMenuItem";
            this.terminalToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.terminalToolStripMenuItem.Text = "Терминал";
            this.terminalToolStripMenuItem.Click += new System.EventHandler(this.terminalToolStripMenuItem_Click);
            // 
            // comPortToolStripMenuItem
            // 
            this.comPortToolStripMenuItem.Name = "comPortToolStripMenuItem";
            this.comPortToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.comPortToolStripMenuItem.Text = "СОМ Порт";
            this.comPortToolStripMenuItem.Click += new System.EventHandler(this.comPortToolStripMenuItem_Click);
            // 
            // modemToolStripMenuItem
            // 
            this.modemToolStripMenuItem.Name = "modemToolStripMenuItem";
            this.modemToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.modemToolStripMenuItem.Text = "Модем";
            this.modemToolStripMenuItem.Click += new System.EventHandler(this.modemToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBtnDisconnect,
            this.toolStripSeparator1,
            this.toolBtnСonPort,
            this.toolBtnСonModem,
            this.toolBtnLogin,
            this.toolButtonPdt});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(462, 35);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolBtnDisconnect
            // 
            this.toolBtnDisconnect.AutoSize = false;
            this.toolBtnDisconnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnDisconnect.Enabled = false;
            this.toolBtnDisconnect.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnDisconnect.Image")));
            this.toolBtnDisconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnDisconnect.Name = "toolBtnDisconnect";
            this.toolBtnDisconnect.Size = new System.Drawing.Size(30, 30);
            this.toolBtnDisconnect.Text = "Отсоединиться";
            this.toolBtnDisconnect.ToolTipText = "Разорвать соединение";
            this.toolBtnDisconnect.Click += new System.EventHandler(this.toolBtnDisconnect_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 35);
            // 
            // toolBtnСonPort
            // 
            this.toolBtnСonPort.AutoSize = false;
            this.toolBtnСonPort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnСonPort.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnСonPort.Image")));
            this.toolBtnСonPort.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnСonPort.Name = "toolBtnСonPort";
            this.toolBtnСonPort.Size = new System.Drawing.Size(30, 30);
            this.toolBtnСonPort.Text = "COM Порт";
            this.toolBtnСonPort.ToolTipText = "Подключиться через COM Порт";
            this.toolBtnСonPort.Click += new System.EventHandler(this.toolBtnСonPort_Click);
            // 
            // toolBtnСonModem
            // 
            this.toolBtnСonModem.AutoSize = false;
            this.toolBtnСonModem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnСonModem.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnСonModem.Image")));
            this.toolBtnСonModem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnСonModem.Name = "toolBtnСonModem";
            this.toolBtnСonModem.Size = new System.Drawing.Size(30, 30);
            this.toolBtnСonModem.Text = "Модем";
            this.toolBtnСonModem.ToolTipText = "Подключиться через Модем";
            this.toolBtnСonModem.Click += new System.EventHandler(this.toolBtnСonModem_Click);
            // 
            // toolBtnLogin
            // 
            this.toolBtnLogin.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolBtnLogin.AutoSize = false;
            this.toolBtnLogin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnLogin.Image = ((System.Drawing.Image)(resources.GetObject("toolBtnLogin.Image")));
            this.toolBtnLogin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnLogin.Name = "toolBtnLogin";
            this.toolBtnLogin.Size = new System.Drawing.Size(30, 30);
            this.toolBtnLogin.Text = "Залогиниться";
            this.toolBtnLogin.ToolTipText = "Войти в систему";
            this.toolBtnLogin.Click += new System.EventHandler(this.toolBtnLogin_Click);
            // 
            // toolButtonPdt
            // 
            this.toolButtonPdt.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolButtonPdt.AutoSize = false;
            this.toolButtonPdt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonPdt.Image = ((System.Drawing.Image)(resources.GetObject("toolButtonPdt.Image")));
            this.toolButtonPdt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonPdt.Name = "toolButtonPdt";
            this.toolButtonPdt.Size = new System.Drawing.Size(30, 30);
            this.toolButtonPdt.Text = "PDT";
            this.toolButtonPdt.ToolTipText = "Войти в PDT";
            this.toolButtonPdt.Click += new System.EventHandler(this.toolButtonPdt_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 435);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(462, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(0, 59);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(462, 376);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 457);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "Terminal";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        public System.Windows.Forms.ToolStripButton toolBtnDisconnect;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;       
        public System.Windows.Forms.ToolStripButton toolBtnLogin;
        private System.Windows.Forms.ToolStripMenuItem actToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terminalToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripMenuItem comPortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modemToolStripMenuItem;
        public System.Windows.Forms.ToolStripButton toolButtonPdt;
        public System.Windows.Forms.ToolStripButton toolBtnСonPort;
        public System.Windows.Forms.ToolStripButton toolBtnСonModem;
    }
}

