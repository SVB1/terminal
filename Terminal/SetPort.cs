﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Terminal
{
    public partial class SetPort : Form
    {
        public SetPort(Form parent)
        {
            this.parent = parent;
            InitializeComponent();
            var str = ComPort.ports;
            if (str.Count == 0)
            {
                var e = new Exception("Не удалось найти порты");
                throw e;
            }
            comboBox1.Items.AddRange(str.ToArray());

            var parity = new ArrayList
            {
                new PortParam("Чётное", "Even"),
                new PortParam("Нечётное", "Odd"),
                new PortParam("Нет", "None"),
                new PortParam("Маркер", "Mark"),
                new PortParam("Пробел", "Space")
            };
            comboBox4.DataSource = parity;
            comboBox4.DisplayMember = "Disp";
            comboBox4.ValueMember = "Data";

            var stopBits = new ArrayList
            {
                new PortParam("1", "One"),
                new PortParam("1.5", "OnePointFive"),
                new PortParam("2", "Two")
            };
            comboBox5.DataSource = stopBits;
            comboBox5.DisplayMember = "Disp";
            comboBox5.ValueMember = "Data";

            var handshake = new ArrayList
            {
                new PortParam("XOn/XOff", "XOnXOff"),
                new PortParam("Аппаратное", "RequestToSend"),
                new PortParam("Нет", "None")
            };
            comboBox6.DataSource = handshake;
            comboBox6.DisplayMember = "Disp";
            comboBox6.ValueMember = "Data";
            if (parent.Name == "MainWindow")
            {
                if (ComPort.Port == null)
                {
                    comboBox2.SelectedItem = "9600";
                    comboBox3.SelectedItem = "8";
                    comboBox4.SelectedValue = "None";
                    comboBox5.SelectedValue = "One";
                    comboBox6.SelectedValue = "None";
                    ok.Enabled = false;
                }
                else
                {
                    comboBox1.SelectedItem = ComPort.Modem.PortName;
                    comboBox2.SelectedItem = ComPort.Port.BaudRate.ToString();
                    comboBox3.SelectedItem = ComPort.Port.DataBits.ToString();
                    comboBox4.SelectedValue = ComPort.Port.Parity.ToString();
                    comboBox5.SelectedValue = ComPort.Port.StopBits.ToString();
                    comboBox6.SelectedValue = ComPort.Port.Handshake.ToString();
                }
            }
            else
            {
                if (ComPort.Modem == null)
                {
                    comboBox2.SelectedItem = "9600";
                    comboBox3.SelectedItem = "8";
                    comboBox4.SelectedValue = "None";
                    comboBox5.SelectedValue = "One";
                    comboBox6.SelectedValue = "None";
                    ok.Enabled = false;
                }
                else
                {
                    comboBox1.SelectedItem = ComPort.Modem.PortName;
                    comboBox2.SelectedItem = ComPort.Modem.BaudRate.ToString();
                    comboBox3.SelectedItem = ComPort.Modem.DataBits.ToString();
                    comboBox4.SelectedValue = ComPort.Modem.Parity.ToString();
                    comboBox5.SelectedValue = ComPort.Modem.StopBits.ToString();
                    comboBox6.SelectedValue = ComPort.Modem.Handshake.ToString();
                }
            }
        }

        private void ok_Click(object sender, EventArgs e)
        {
            if (parent.Name == "MainWindow")
            {
                ComPort.Port = new ComPort(comboBox1.SelectedItem.ToString())
                {
                    BaudRate = Convert.ToInt32(comboBox2.SelectedItem),
                    DataBits = Convert.ToInt32(comboBox3.SelectedItem),
                    Parity = (Parity) Enum.Parse(typeof (Parity),
                        comboBox4.SelectedValue.ToString()),
                    StopBits = (StopBits) Enum.Parse(typeof (StopBits),
                        comboBox5.SelectedValue.ToString()),
                    Handshake = (Handshake) Enum.Parse(typeof (Handshake),
                        comboBox6.SelectedValue.ToString()),
                    ReadTimeout = 100,
                    WriteTimeout = 100
                };
                //Сохраняем значения
                Properties.Settings.Default.Port_Name = ComPort.Port.PortName;
                Properties.Settings.Default.Port_BaudRate = ComPort.Port.BaudRate;
                Properties.Settings.Default.Port_DataBits = ComPort.Port.DataBits;
                Properties.Settings.Default.Port_Parity = ComPort.Port.Parity.ToString();
                Properties.Settings.Default.Port_StopBits = ComPort.Port.StopBits.ToString();
                Properties.Settings.Default.Port_Handshake = ComPort.Port.Handshake.ToString();
                Properties.Settings.Default.Save();
                Program.MainWindow.toolBtnСonPort.Enabled = true;
                Close();
            }
            else
            {
                ComPort.Modem = new ComPort(comboBox1.SelectedItem.ToString())
                {
                    BaudRate = Convert.ToInt32(comboBox2.SelectedItem),
                    DataBits = Convert.ToInt32(comboBox3.SelectedItem),
                    Parity = (Parity) Enum.Parse(typeof (Parity),
                        comboBox4.SelectedValue.ToString()),
                    StopBits = (StopBits) Enum.Parse(typeof (StopBits),
                        comboBox5.SelectedValue.ToString()),
                    Handshake = (Handshake) Enum.Parse(typeof (Handshake),
                        comboBox6.SelectedValue.ToString()),
                    ReadTimeout = 100,
                    WriteTimeout = 100
                };
                modem = parent as SetModem;
                modem.labelPort.Text = ComPort.Modem.PortName;
                //Сохраняем значения
                Properties.Settings.Default.Modem_Name = ComPort.Modem.PortName;
                Properties.Settings.Default.Modem_BaudRate = ComPort.Modem.BaudRate;
                Properties.Settings.Default.Modem_DataBits = ComPort.Modem.DataBits;
                Properties.Settings.Default.Modem_Parity = ComPort.Modem.Parity.ToString();
                Properties.Settings.Default.Modem_StopBits = ComPort.Modem.StopBits.ToString();
                Properties.Settings.Default.Modem_Handshake = ComPort.Modem.Handshake.ToString();
                Properties.Settings.Default.Save();
                Close();
            }
        }

        private void cansel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null) return;
            ok.Enabled = true;
        }

        private Form parent;
        private SetModem modem;
    }

    ///<summary>Определяет свойства для значений комбобоксов параметров порта</summary>
    public class PortParam
    {
        private string _disp;
        private string _data;

        public PortParam(string strDisp, string strData)
        {
            _disp = strDisp;
            _data = strData;
        }

        public string Disp
        {
            get { return _disp; }
        }

        public string Data
        {
            get { return _data; }
        }
    }
}