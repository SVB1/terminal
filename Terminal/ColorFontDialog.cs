﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Terminal
{
    public partial class ColorFontDialog : Form
    {
        public ColorFontDialog()
        {
            InitializeComponent();
            color1 = mainWindow.richTextBox1.BackColor;
            color2 = mainWindow.richTextBox1.ForeColor;
            color3 = mainWindow.richTextBox1.SelectionBackColor;
            font = mainWindow.richTextBox1.Font;
            textBox1.Text = font.Name;
            richTextBox1.Font = font;
            button1.BackColor = color1;
            button2.BackColor = color2;
            button3.BackColor = Color.Snow;
            button4.BackColor = Color.DodgerBlue;
            richTextBox1.BackColor = color1;
            richTextBox1.ForeColor = color2;
            richTextBox1.SelectionBackColor = color3;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = color1;
            if (colorDialog1.ShowDialog()==DialogResult.OK)
            {
                color1 = colorDialog1.Color;
                richTextBox1.BackColor = color1;
                button1.BackColor = color1;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = color2;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                color2 = colorDialog1.Color;
                richTextBox1.ForeColor = color2;
                button2.BackColor = color2;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = font;
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                font = fontDialog1.Font;
                textBox1.Text = font.Name;
                richTextBox1.Font = font;
                textBox1.Font = font;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            mainWindow.richTextBox1.Font = font;
            mainWindow.richTextBox1.BackColor = color1;
            mainWindow.richTextBox1.ForeColor = color2;
            this.Close();
        }

    }
}
