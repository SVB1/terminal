﻿using System;
using System.Collections;
using System.IO;
using System.IO.Ports;
using System.Security.Cryptography;
using System.Threading;
using System.Windows.Forms;
using Timer = System.Threading.Timer;

namespace Terminal
{
    ///<summary> Объект СОМ порта </summary>
    public class ComPort : SerialPort
    {
        private static ArrayList _ports;
        public static MainWindow MainWindow;
        public static ComPort Port;
        public static ComPort Modem;
        public static ComPort Connected;
        public static string ModemInit = "";
        public static string ModemDial = "ATDT";
        public static string LastModemDial;
        private bool _continue;
        private Thread _readThread;
        private bool _timerTick = false;
        private string _tempStr;
        private Timer timer;

        ///<summary>Конструктор</summary>
        public ComPort(string s)
            : base(s)
        {
        }

        public static ArrayList ports
        {
            get
            {
                listPorts();
                return _ports;
            }
        }

        ///<summary> Создаёт список СОМ портов в системе  </summary>
        private static void listPorts()
        {
            _ports = new ArrayList();
            for (var i = 1; i < 20; i++)
            {
                var name = "COM" + Convert.ToString(i);
                try
                {
                    var port = new SerialPort(name);
                    port.Open();
                    port.Close();
                    _ports.Add(name);
                }
                catch (IOException)
                {
                    //Console.WriteLine("EROR"); 
                }
            }
        }

        ///<summary> Подключиться к порту </summary>
        public void connect()
        {
            try
            {
                Open();
                _readThread = new Thread(_read);
                _continue = true;
                Connected = this;
                MainWindow.richTextBox1.KeyPress += textBox_KeyPress;
                _readThread.Start();   
                MainWindow.toolBtnСonPort.Enabled = false;
                MainWindow.toolBtnСonModem.Enabled = false;
                MainWindow.toolBtnDisconnect.Enabled = true;
                MainWindow.toolBtnLogin.Enabled = true;
                MainWindow.toolButtonPdt.Enabled = true;
                if (this == Modem)
                {
                    ThreadPool.QueueUserWorkItem(modemDial);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Не удаётся открыть порт", "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ///<summary>Метод для набора номера модемом</summary>
        private void modemDial(object st)
        {
            if (ModemInit != "")
            {
                _write(ModemInit + "\r");
                Thread.Sleep(300);
            }
            _write(ModemDial + LastModemDial + "\r");
        }

        ///<summary> Отключиться </summary>
        public void disconnect()
        {
            _continue = false;
            _readThread.Join();
            MainWindow.richTextBox1.KeyPress -= textBox_KeyPress;
            Close();
            Connected = null;
            MainWindow.Invoke((MethodInvoker) (delegate { MainWindow.portClose(); }));
        }

        public static void modemDisconnect(object st)
        {
            Connected._write("+++");
            Thread.Sleep(700);
            Connected._write("ATH0\r");
            Thread.Sleep(300);
            Connected.disconnect();
        }

        ///<summary> Поток чтения из порта </summary>
        private void _read()
        {
            while (_continue)
            {
                try
                {
                    var s = new string(Convert.ToChar(ReadChar()), 1);
                    if (_timerTick) _tempStr += s;
                    if (s == "\n") continue;
                    MainWindow.richTextBox1.Invoke((MethodInvoker)
                        (delegate { MainWindow.richTextBox1.AppendText(s); }));
                }
                catch (TimeoutException)
                {
                }
                catch (InvalidOperationException)
                {
                    MessageBox.Show("Ошибка чтения из порта", "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        ///<summary>Запись строки в порт</summary>
        public void _write(string s)
        {
            try
            {
                Write(s);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка записи в порт", "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ///<summary>Обработчик событий клавиатуры</summary>
        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            _write(e.KeyChar.ToString());
            //Console.WriteLine((int)e.KeyChar);     todo
        }

        ///<summary>Метод выводит strOut1, ждет strIn и выводит strOut2</summary>
        public void strWait(object obg)
        {
            var d = (StrWaitData) obg;
            _timerTick = true;
            _tempStr = "";
            _write(d.StrOut1);
            timer = new Timer(timerCallback, null, d.Timeout, 0);
            while (_timerTick)
            {
                Thread.Sleep(100);
                if (_tempStr.Contains(d.StrIn))
                {
                    _write(d.StrOut2);
                    timer.Dispose();
                    break;
                }
            }
        }

        private void timerCallback(object o)
        {
            _timerTick = false;
            timer.Dispose();
            MessageBox.Show("Превышено время ожидания!", "Ошибка!",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
    }

    public class StrWaitData
    {
        public StrWaitData(string strIn, string strOut1, string strOut2, int timeout = 1000)
        {
            StrIn = strIn;
            StrOut1 = strOut1;
            StrOut2 = strOut2;
            Timeout = timeout;
        }

        public readonly string StrIn;
        public readonly string StrOut1;
        public readonly string StrOut2;
        public readonly int Timeout;
    }
}