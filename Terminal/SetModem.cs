﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Terminal
{
    public partial class SetModem : Form
    {
        public SetModem()
        {
            InitializeComponent();
            if ((ComPort.Modem != null) && (ComPort.ports.Contains(ComPort.Modem.PortName)))
            {
                 labelPort.Text = ComPort.Modem.PortName;
            }
            else
            {
                labelPort.Text = "Не выбран";
            }
            textBox1.Text = ComPort.ModemInit;
            comboBox2.SelectedItem = ComPort.ModemDial=="ATDT" ? "Тональный" : "Импульсный";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ComPort.ModemDial = comboBox2.SelectedItem.ToString() == "Тональный" ? "ATDT" : "ATDP";
            ComPort.ModemInit = textBox1.Text;
            Properties.Settings.Default.Modem_Dial = ComPort.ModemDial;
            Properties.Settings.Default.Modem_Init = ComPort.ModemInit;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var form = new SetPort(this);
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}