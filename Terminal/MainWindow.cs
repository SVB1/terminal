﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Terminal
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            toolBtnLogin.Enabled = false;
            toolButtonPdt.Enabled = false;
            FormClosing += Form1_FormClosing;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Location = Properties.Settings.Default.Align;
            ClientSize = Properties.Settings.Default.Size;
            richTextBox1.BackColor = Properties.Settings.Default.TermBkgColor;
            richTextBox1.ForeColor = Properties.Settings.Default.TermTextColor;
            richTextBox1.Font = Properties.Settings.Default.TermFont;
            richTextBox1.SelectionStart = richTextBox1.TextLength;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ComPort.Connected != null) ComPort.Connected.disconnect();
            Properties.Settings.Default.Align = Location;
            Properties.Settings.Default.Size = ClientSize;
            Properties.Settings.Default.TermBkgColor = richTextBox1.BackColor;
            Properties.Settings.Default.TermTextColor = richTextBox1.ForeColor;
            Properties.Settings.Default.TermFont = richTextBox1.Font;
            Properties.Settings.Default.Last_Dial = ComPort.LastModemDial;
            Properties.Settings.Default.Save();
        }

        private void toolBtnСonPort_Click(object sender, EventArgs e)
        {
            ComPort.Port.connect();
        }

        private void toolBtnDisconnect_Click(object sender, EventArgs e)
        {
            if (ComPort.Connected == ComPort.Modem) ThreadPool.QueueUserWorkItem(ComPort.modemDisconnect);
            else ComPort.Connected.disconnect();
        }

        private void toolBtnСonModem_Click(object sender, EventArgs e)
        {
            var form = new ModemDialNamber();
            form.ShowDialog(this);
        }

        private void terminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ColorFontDialog();
            form.ShowDialog(this);
        }

        private void comPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var form = new SetPort(this);
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void modemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new SetModem();
            form.ShowDialog(this);
        }

        private void toolBtnLogin_Click(object sender, EventArgs e)
        {
            var data = new StrWaitData("PASS?", "logi admin1\r", "0000\r");
            ThreadPool.QueueUserWorkItem(ComPort.Connected.strWait, data);
        }

        ///<summary>При закрытии порта настраивает кнопки</summary>
        public void portClose()
        {
            toolBtnDisconnect.Enabled = false;
            toolBtnLogin.Enabled = false;
            toolButtonPdt.Enabled = false;
            if (ComPort.Port != null) toolBtnСonPort.Enabled = true;
            if (ComPort.Modem != null) toolBtnСonModem.Enabled = true;
        }

        private void toolButtonPdt_Click(object sender, EventArgs e)
        {
            var data = new StrWaitData("Password:", "\u0010\u0004\u0014", "2tdp22ler\r");
            ThreadPool.QueueUserWorkItem(ComPort.Connected.strWait, data);
        }
    }
}