﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Terminal
{
    public partial class ModemDialNamber : Form
    {
        public ModemDialNamber()
        {
            InitializeComponent();
            textBox1.Text = ComPort.LastModemDial;
            textBox1.SelectAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                ComPort.LastModemDial = textBox1.Text;
                ComPort.Modem.connect();
            }
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
