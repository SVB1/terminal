﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Terminal
{
    internal static class Program
    {
        ///<summary> Главная точка входа для приложения. </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainWindow = new MainWindow();
            ColorFontDialog.mainWindow = MainWindow;
            ComPort.MainWindow = MainWindow;
            portLoad();
            Application.Run(MainWindow);
        }

        ///<summary>Загрузка параметров порта</summary>
        private static void portLoad()
        {
            var arr = ComPort.ports;
            if (arr.Contains(Properties.Settings.Default.Port_Name))
            {
                ComPort.Port = new ComPort(Properties.Settings.Default.Port_Name)
                {
                    BaudRate = Properties.Settings.Default.Port_BaudRate,
                    DataBits = Properties.Settings.Default.Port_DataBits,
                    Parity = (Parity) Enum.Parse(typeof (Parity),
                        Properties.Settings.Default.Port_Parity),
                    StopBits = (StopBits) Enum.Parse(typeof (StopBits),
                        Properties.Settings.Default.Port_StopBits),
                    Handshake = (Handshake) Enum.Parse(typeof (Handshake),
                        Properties.Settings.Default.Port_Handshake),
                    ReadTimeout = 100,
                    WriteTimeout = 100
                };
            }
            else
            {
                MainWindow.toolBtnСonPort.Enabled = false;
            }

            if (arr.Contains(Properties.Settings.Default.Modem_Name))
            {
                ComPort.Modem = new ComPort(Properties.Settings.Default.Modem_Name)
                {
                    BaudRate = Properties.Settings.Default.Modem_BaudRate,
                    DataBits = Properties.Settings.Default.Modem_DataBits,
                    Parity = (Parity)Enum.Parse(typeof(Parity),
                        Properties.Settings.Default.Modem_Parity),
                    StopBits = (StopBits)Enum.Parse(typeof(StopBits),
                        Properties.Settings.Default.Modem_StopBits),
                    Handshake = (Handshake)Enum.Parse(typeof(Handshake),
                        Properties.Settings.Default.Modem_Handshake),
                    ReadTimeout = 100,
                    WriteTimeout = 100
                };
            }
            else
            {
                MainWindow.toolBtnСonModem.Enabled = false;
            }
            ComPort.ModemDial = Properties.Settings.Default.Modem_Dial;
            ComPort.ModemInit = Properties.Settings.Default.Modem_Init;
            ComPort.LastModemDial = Properties.Settings.Default.Last_Dial;
        }
        public static MainWindow MainWindow;
    }
}